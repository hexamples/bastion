# Bastion
Instancia de operaciones con acceso externo.<br>
Por defecto la instancia está en una subred pública pero se puede cambiar a que sea en una privada.<br>
El acceso es por Session Manager, por lo cual no tiene una llave ssh ni el puerto 22 abierto.
