resource "aws_iam_instance_profile" "bastion" {
  name = "${var.scope_name}-bastion"
  role = aws_iam_role.bastion.name

  tags = local.tags
}

resource "aws_iam_role" "bastion" {
  name = "${var.scope_name}-bastion"
  path = "/"

  assume_role_policy = <<-EOT
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Action": "sts:AssumeRole",
        "Principal": {
          "Service": "ec2.amazonaws.com"
        },
        "Effect": "Allow",
        "Sid": ""
      }
    ]
  }
  EOT

  tags = local.tags
}

resource "aws_iam_role_policy_attachment" "bastion_ssm" {
  role       = aws_iam_role.bastion.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}
