variable "scope_name" { default = "" }
variable "tags" { default = {} }

variable "remote_state_default_http_username" {}  # viene en el entorno
variable "remote_state_default_http_password" {}  # viene en el entorno
variable "remote_state_networking_http_address" {}

variable "instance_type" { default = "t3.micro" }
variable "root_volume_size" { default = 8 }
variable "place_in_public_subnet" { default = true }

variable "ssh_allow_cidr" { default = [] }
