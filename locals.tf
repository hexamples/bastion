locals {
  tags = merge(var.scope_name != "" ? { scope = var.scope_name } : {}, var.tags)
}
