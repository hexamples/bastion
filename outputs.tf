output "public_ip_address" {
  value = aws_instance.bastion_instance.public_ip
}
