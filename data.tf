data "aws_region" "current" {}

data "terraform_remote_state" "networking" {
  backend = "http"
  config = {
    username = var.remote_state_default_http_username
    password = var.remote_state_default_http_password
    address  = var.remote_state_networking_http_address
  }
}

data "aws_ami" "ubuntu" {
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["099720109477"] # Canonical
}
