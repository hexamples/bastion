resource "aws_instance" "bastion_instance" {
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = var.instance_type
  monitoring             = true
  vpc_security_group_ids = [module.bastion_sg.security_group_id]
  subnet_id              = (
    var.place_in_public_subnet ?
    data.terraform_remote_state.networking.outputs.vpc_public_subnets[0] : 
    data.terraform_remote_state.networking.outputs.vpc_private_subnets[0]
  )

  associate_public_ip_address = var.place_in_public_subnet

  iam_instance_profile   = aws_iam_instance_profile.bastion.name

  root_block_device {
    encrypted   = true
    volume_type = "gp3"
    volume_size = var.root_volume_size
  }

  tags = merge({
    Name  = "${var.scope_name}-bastion"
  }, local.tags)

  lifecycle {
    ignore_changes = [ami]
  }
}

module "bastion_sg" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 4"

  name        = "${var.scope_name}-bastion-sg"
  description = "Allow remote access"
  vpc_id      = data.terraform_remote_state.networking.outputs.vpc_id
  
  egress_rules = ["all-all"]

  tags = local.tags
}
